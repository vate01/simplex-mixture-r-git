
library(Rcpp)
library(inline)
library(RcppArmadillo)
library(boot)
sourceCpp("./extra/simplex_mixture.cpp")

# simplexEM <- function(y, K, tol,...) {
simplexEM <- function(y, K, tol, init=NULL, pj=NULL, pij=NULL, restarts=10, ...) {  

  N <- length(y)
  # pj <- VGAM::rdiric(1, rep(1, K))
  # pij <- VGAM::rdiric(N, rep(1, K))
  # para <- c(runif(K), rep(1, K))
  # para <- c(sort(as.numeric(kmeans(y, 3)$centers)), rep(1, K))
  # para_old <- c(runif(K), rep(1, K))  # rep(1, K)
  
  aux<-kmeans(y, K)
  
  # link(parametro)=covariables
  # parametro=link_inverso(covariables)
  # ifelse(is.null(init), para <- c(logit(as.numeric(tapply(y,aux$cluster,mean))), log(as.numeric(tapply(y,aux$cluster,var)))), para<-init)
  
  medias<-tapply(y, aux$cluster, mean)
  if(is.null(init))
  {
    para <- logit(as.numeric(medias))
    for(i in 1:K)
    {
      para<-c(para, log(sigma2_estim(y[aux$cluster==i], medias[i])) )
    }
  } else {
    para<-init
  }
  
  para0<-para
  
  if(is.null(pj)) pj <- c(as.numeric(table(aux$cluster)/length(y)))
  if(is.null(pij))
  {
    pij<-peij(y, c(as.numeric(table(aux$cluster)/length(y))), 
              c((as.numeric(aux$centers)), (rep(.1, K))) )
    
    pij[which(is.nan(pij))]<-1    
  }
  
  para_old<-para
  
  loglik <- 1  
  
  cont <- 0
  diferencia <- 1
  
  while (diferencia > tol) {
    para_old <- para
    loglik_old <- loglik
    # if(sum(pj==0)>0) # break
    # {
    restarings<-0
    saltar <- 0
    while(sum(pj==0)!=0 & restarings<=restarts)
    {
      pj<-runif(K)
      temp <- optim(para, fn, y = y, pj = pj, pij = pij, control = list(maxit = 1000), ...)
      saltar<-1
      rstartings<-restarings+1
    }
    # }
    
    if(saltar==0) temp <- optim(para, fn, y = y, pj = pj, pij = pij, control = list(maxit = 1000), ...)  
    
    para <- temp$par
    loglik <- fn(y, pj, c(para[1:K], para[-c(1:K)]), pij) # log scale
    
    diferencia <- abs(loglik - loglik_old)
    
    pj <- apply(pij, 2, mean)
    pij <- peij(y, pj, c(inv.logit(para[1:K]),exp(para[-c(1:K)])) )
    
    cont <- cont + 1
  }
  
  se <- NULL
  if(!is.null(temp$hessian)) se <- sqrt(diag(solve(temp$hes)))
  loglik <- fn(y, pj, c((para[1:K]), (para[-c(1:K)])), pij) # log scale
  loglik_ind <- fn_ind(y, pj, c((para[1:K]), (para[-c(1:K)])), pij) # log scale
  classs <- apply(pij, 1, which.max)
  
  return(list(mu = inv.logit(para[1:K]), se_mu = se[1:K], sigma = exp(para[-c(1:K)]), se_sigma = se[-c(1:K)], 
              pj = pj, pij = pij, loglik = loglik, loglik_ind= loglik_ind, class = classs, cont=cont, para0=para0,
              convergence=temp$convergence, restarings=restarings))
}
