# modelling sources:
library(Rcpp)
library(inline)
library(RcppArmadillo)
sourceCpp("./extra/zim_beta_mixture.cpp")

betaEM_zim <- function(y, K, tol, init=NULL, pj=NULL, pij=NULL, restarts=10, ...) {
  
  cont <- 0
  diferencia <- 1
  # N <- 0
  # K<-3
  N <- length(y)
  # pj <- VGAM::rdiric(1, rep(1, K + 1))
  # pij <- VGAM::rdiric(N, rep(1, K + 1))
  aux<-kmeans(y[y!=0], K)

  # aquí
  medias<-tapply(y[y!=0], aux$cluster, mean)
  if(is.null(init))
  {
    para <- logit(as.numeric(medias))
    for(i in 1:K)
    {
      para<-c(para, log(sigma2_estim(y[aux$cluster==i], medias[i])) )
    }
  } else {
    para<-init
  }
  
  para0<-para

  if(is.null(pj)) pj <- c(mean(y==0), as.numeric(table(aux$cluster)/length(y)))
  if(is.null(pij))
  {
    pij<-peij_zim(y, c(mean(y==0), as.numeric(table(aux$cluster)/length(y))), 
                  c((as.numeric(aux$centers)), (rep(.1, K))) )
    
    pij[is.nan(apply(pij,1,sum)),2:(K+1)]<-VGAM::rdiric(1,rep(1,K))
  }
  
  # para <- c(runif(K), rep(1, K))
  para_old <- para # c(runif(K), rep(1, K))
  # value <- 1
  loglik <- 1
  
  while (diferencia > tol) {
    # para_old <- para
    loglik_old <- loglik
    # temp <- optim(para, fn_zim_beta, y = y, pj = pj, pij = pij, control = list(maxit = 1000), ...) # method = "BFGS", hessian = T)
    # if(sum(pj==0)>0) # break
    # {
    restartings<-0
    saltar <- 0
    while(sum(pj==0)!=0 & restartings<=restarts)
    {
      pj<-runif(K)
      # temp <- optim(para, fn, y = y, pj = pj, pij = pij, control = list(maxit = 1000), ...)
      temp <- optim(para, fn_zim_beta, y = y, pj = pj, pij = pij, control = list(maxit = 1000), ...) # method = "BFGS", hessian = T)
      saltar<-1
      restartings<-restartings+1
    }
    # }
    
    if(saltar==0) temp <- optim(para, fn_zim_beta, y = y, pj = pj, pij = pij, control = list(maxit = 1000), ...) # method = "BFGS", hessian = T)
    
    para <- temp$par
    
    # diferencia <- as.numeric(dist(rbind(para, para_old)))
    loglik <- fn_zim_beta(y, pj, c(para[1:K], para[-c(1:K)]), pij) # log scale
    diferencia <- abs(loglik - loglik_old)
    
    # pij <- peij_zim_beta(y, pj, para)
    pij <- peij_zim_beta(y, pj, c(inv.logit(para[1:K]), exp(para[-c(1:K)])) )
    
    cont <- cont + 1
  }
  
  # para <- temp$par
  se <- NULL
  if(!is.null(temp$hessian)) se <- sqrt(diag(solve(temp$hes)))
  loglik <- fn_zim_beta(y, pj, c(para[1:K], para[-c(1:K)]), pij) # log scale
  loglik_ind <- fn_zim_ind_beta(y, pj, c(para[1:K], para[-c(1:K)]), pij) # log scale
  classs <- apply(pij, 1, which.max)
  
  return(list(mu = inv.logit(para[1:K]), se_mu = se[1:K], sigma = exp(para[-c(1:K)]), se_sigma = se[-c(1:K)], 
        cont=cont, pj = pj, pij = pij, loglik = loglik, loglik_ind= loglik_ind, class = classs, para0=para0,
        convergence=temp$convergence, restartings=restartings))
}
