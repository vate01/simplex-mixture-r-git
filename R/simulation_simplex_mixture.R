
library(Rcpp)
library(inline)
library(RcppArmadillo)
library(caret)
sourceCpp("./extra/simplex_mixture.cpp")
sourceCpp("./extra/zim_simplex_mixture.cpp")
source("./R/simplex_mixture_cpp.R")
source("./R/zim_simplex_mixture_cpp.R")
source("./extra/extra_functions.R")

###########

# http://stats.stackexchange.com/questions/70855/generating-random-variables-from-a-mixture-of-normal-distributions
simulacion_K_grupos<-function(mus, sigmas, Ene, MonteCarlo, tol, 
  init=NULL, pj=NULL, pij=NULL, ...)
{
  K<-length(mus)
  
  estimado_media<-matrix(nrow=MonteCarlo,ncol=K)
  estimado_dispersion<-matrix(nrow=MonteCarlo,ncol=K)
  diff_medias<-matrix(nrow=MonteCarlo,ncol=K)
  diff_dispersion<-matrix(nrow=MonteCarlo,ncol=K)
  estimado_correctos<-numeric(MonteCarlo)
  estimado_sensibilidad<-numeric(MonteCarlo)
  estimado_especificidad<-numeric(MonteCarlo)
  estimado_hellinger<-numeric(MonteCarlo)
  
  # ind<-c()
  # for(k in 1:length(Ene)) ind<-c(ind, rep(k, Ene[k]))
  ind <- rep(1:length(Ene), Ene)
  
  par1_aux<-sort(mus, decreasing = T)
  par2_aux<-c(sigmas)[order(mus, decreasing=T)]
  modelo <- simplexEM(runif(100), 2, .1)
  error_flag<-0
  contador<-0
  while(contador<MonteCarlo)
  {
    
    modelo_<-try(1+'a', silent=T)
    while(class(modelo_)=='try-error' | sum(modelo$sigma>20)>0 )
    {
      # y<-c()
      # for(k in 1:K)  y<-c(y, VGAM::rsimplex(Ene[k], mus[k], sigmas[k]))
      y <- mezcla(Ene = Ene, mus = mus, sigmas = sigmas)
      modelo <- try(simplexEM(y, K, tol, init, pj, pij, ...), silent=T)
      modelo_ <- modelo
      if(class(modelo)=='try-error') modelo<-simplexEM(runif(100), 2, .1)
      error_flag <- error_flag + 1
    }
    
    aux22<-confusionMatrix(ind, sort(modelo$class))
    # estimado_correctos[contador+1] <- sum(diag( table(sort(modelo$class), ind) ))/length(y)
    estimado_correctos[contador+1] <- as.numeric(aux22$overall['Accuracy'])
    estimado_sensibilidad[contador+1] <- as.numeric(aux22$byClass)[1]
    estimado_especificidad[contador+1] <- as.numeric(aux22$byClass)[2]
    estimado_hellinger[contador+1] <- emp_hellinger(y, modelo)
    estimado_media[contador+1,] <- sort(modelo$mu, decreasing = T)
    estimado_dispersion[contador+1,]<-modelo$sigma[order(modelo$mu, decreasing = T)]
    diff_medias[contador+1,]<-sort(modelo$mu, decreasing = T)-par1_aux 
    diff_dispersion[contador+1,]<-modelo$sigma[order(modelo$mu, decreasing = T)]- par2_aux 
    contador<-contador+1
  }
  
  ii<-mean(estimado_correctos)
  aa<-apply(diff_medias,2,mean)
  bb<-apply(diff_dispersion,2,mean)
  
  ee<-apply((diff_medias)^2,2,mean)
  ff<-apply((diff_dispersion)^2,2,mean)
  
  gg<-apply(estimado_media,2,mean)
  hh<-apply(estimado_dispersion,2,mean)
  
  cc<-abs(((gg-par1_aux)/par1_aux)*100)
  dd<-abs(((hh-par2_aux)/par2_aux)*100)
  
  jj<-mean(estimado_sensibilidad)
  kk<-mean(estimado_especificidad)
  
  ll<-mean(estimado_hellinger)
  
  return(list(tt=cbind(real=c(par1_aux,par2_aux),
    estim=c(gg,hh),
      bias=c(aa,bb),rel=c(cc,dd),mse=c(ee,ff)),ii=ii,jj=jj,kk=kk,ll=ll,
        error_flag=error_flag-MonteCarlo))

}

######################################
######################################

uno<-simulacion_K_grupos(mus = c(.6, .9), sigmas = c(.5, 1),
  Ene = c(50,50), MonteCarlo = 100, tol = .1, method='BFGS', hessian=F)
# dput(x=uno, file='./exported-files/simulation_1/uno.txt')

dos<-simulacion_K_grupos(mus = c(.6, .9), sigmas = c(.5, 1),
  Ene = c(100,100), MonteCarlo = 100, tol = .1, method='BFGS',hessian=F)
# dput(x=dos, file='./exported-files/simulation_1/dos.txt')

tres<-simulacion_K_grupos(mus = c(.6, .9), sigmas = c(.5, 1),
  Ene = c(500,500), MonteCarlo = 100, tol = .1, method='BFGS',hessian=F)
# dput(x=tres, file='./exported-files/simulation_1/tres.txt')

cuatro<-simulacion_K_grupos(mus = c(.6, .8, .9),  sigmas=c(.5, .5, 1),
  Ene = c(50,50,50), MonteCarlo = 100, tol = .1, method='BFGS',hessian=F)
# dput(x=cuatro, file='./exported-files/simulation_1/cuatro.txt')

cinco<-simulacion_K_grupos(mus = c(.6, .8, .9),  sigmas=c(.5, .5, 1),
  Ene = c(100,100,100), MonteCarlo = 100, tol = .1, method='BFGS',hessian=F)
# dput(x=cinco, file='./exported-files/simulation_1/cinco.txt')

seis<-simulacion_K_grupos(mus = c(.6, .8, .9),  sigmas=c(.5, .5, 1),
  Ene = c(500,500,500), MonteCarlo = 100, tol = .1, method='BFGS',hessian=F)
# dput(x=seis, file='./exported-files/simulation_1/seis.txt')

siete<-simulacion_K_grupos(mus = c(.4, .6, .8, .9),  sigmas=c(.1, .5, .5, 1),
  Ene = c(50, 50, 50, 50), MonteCarlo = 100, tol = .1, method='BFGS', hessian=F)
# dput(x=siete, file='./exported-files/simulation_1/siete.txt')

ocho<-simulacion_K_grupos(mus = c(.4, .6, .8, .9),  sigmas=c(.1, .5, .5, 1),
  Ene = c(100, 100, 100, 100), MonteCarlo = 100, tol = .1, method='BFGS',hessian=F)
# dput(x=ocho, file='./exported-files/simulation_1/ocho.txt')

nueve<-simulacion_K_grupos(mus = c(.4, .6, .8, .9),  sigmas=c(.1, .5, .5, 1),
  Ene = c(500, 500, 500, 500), MonteCarlo = 100, tol = .1, method='BFGS',hessian=F)
# dput(x=nueve, file='./exported-files/simulation_1/nueve.txt')

diez<-simulacion_K_grupos(mus = c(.3, .4, .6, .8, .9),  sigmas=c(1, .1, .5, .5, 1),
  Ene = c(50, 50, 50, 50, 50), MonteCarlo = 100, tol = .1, method='BFGS', hessian=F)
# dput(x=diez, file='./exported-files/simulation_1/diez.txt')

once<-simulacion_K_grupos(mus = c(.3, .4, .6, .8, .9),  sigmas=c(1, .1, .5, .5, 1),
  Ene = c(100, 100, 100, 100, 100), MonteCarlo = 100, tol = .1, method='BFGS',hessian=F)
# dput(x=once, file='./exported-files/simulation_1/once.txt')

doce<-simulacion_K_grupos(mus = c(.3, .4, .6, .8, .9),  sigmas=c(1, .1, .5, .5, 1),
  Ene = c(500, 500, 500, 500, 500), MonteCarlo = 100, tol = .1, method='BFGS',hessian=F)
# dput(x=doce, file='./exported-files/simulation_1/doce.txt')

uno<-dget('./exported-files/simulation_1/uno.txt')
dos<-dget('./exported-files/simulation_1/dos.txt')
tres<-dget('./exported-files/simulation_1/tres.txt')
cuatro<-dget('./exported-files/simulation_1/cuatro.txt')
cinco<-dget('./exported-files/simulation_1/cinco.txt')
seis<-dget('./exported-files/simulation_1/seis.txt')
siete<-dget('./exported-files/simulation_1/siete.txt')
ocho<-dget('./exported-files/simulation_1/ocho.txt')
nueve<-dget('./exported-files/simulation_1/nueve.txt')
diez<-dget('./exported-files/simulation_1/diez.txt')
once<-dget('./exported-files/simulation_1/once.txt')
doce<-dget('./exported-files/simulation_1/doce.txt')

# parameters:
round(rbind(
  cbind(uno$tt[,-3],dos$tt[,-c(1,3)],tres$tt[,-c(1,3)]),
  cbind(cuatro$tt[,-3],cinco$tt[,-c(1,3)],seis$tt[,-c(1,3)]),
  cbind(siete$tt[,-3],ocho$tt[,-c(1,3)],nueve$tt[,-c(1,3)]),
  cbind(diez$tt[,-3],once$tt[,-c(1,3)],doce$tt[,-c(1,3)])
),2)

# Accuracy
rbind(
  c(uno$ii,dos$ii,tres$ii),
  c(cuatro$ii,cinco$ii,seis$ii),
  c(siete$ii,ocho$ii,nueve$ii),
  c(diez$ii,once$ii,doce$ii)
)

# sensitivity
rbind(
  c(uno$jj,dos$jj,tres$jj),
  c(cuatro$jj,cinco$jj,seis$jj),
  c(siete$jj,ocho$jj,nueve$jj),
  c(diez$jj,once$jj,doce$jj)
)

# specificity
rbind(
  c(uno$kk,dos$kk,tres$kk),
  c(cuatro$kk,cinco$kk,seis$kk),
  c(siete$kk,ocho$kk,nueve$kk),
  c(diez$kk,once$kk,doce$kk)
)

# Hellinger distance
rbind(
  c(uno$ll,dos$ll,tres$ll),
  c(cuatro$ll,cinco$ll,seis$ll),
  c(siete$ll,ocho$ll,nueve$ll),
  c(diez$ll,once$ll,doce$ll)
)
