This folder contains two examples to estimate a simplex mixture model. The first uses bioinformatic data and the second one employ an MRI image. It has been tested on R (3.4.0). It is a requirement have installed the package Rcpp and RcppArmadillo. Package betareg, mixtools and ggplot2 are optional because the very first are needed to compare with the simplex mixture model and the last one is just to have a nicer histogram. The png library is also needed to read the png file.

To run it, R must be placed where this README file is. Both examples are in folder /R., i.e: R/bioinformatic.R and R/image.R.

