**Mezcla de distribuciones simplex.**

Los objetivos son utilizarlas en distribuciones que estén restringidas a un intervalo determinado.

Se incluye mezcla de distribuciones simplex-{0,1}-infladas con aplicación a imágenes.

* `fn`, del archivo `simplex_mixture.cpp`, arroja resultados con el signo en negativo y en logaritmos. Este está asociado al valor `loglik` de la función principal; esto es, es más conveniente maximizar el negativo de la función de verosimilitud. De manera que si se quiere escoger el modelo con la verosimilitud más alta, debe ser el del valor más bajo. Si se le cambia el signo, entonces debe ser el modelo con el valor más alto.
* `fn_ind`, del archivo `simplex_mixture.cpp`, arroja resultados con el signo real.
