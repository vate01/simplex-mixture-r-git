#include <RcppArmadillo.h>
using namespace Rcpp;
// [[Rcpp::depends("RcppArmadillo")]]
#include <RcppArmadilloExtensions/sample.h>


// [[Rcpp::export]]
arma::vec deeFun(arma::vec y, double mu) {

    arma::vec A= pow((y - mu)/(mu * (1 - mu)),2) ;
    arma::vec B= (y % (1 - y));
    arma::vec C=A/B;

    return(C);
}  
    
// [[Rcpp::export]]
arma::vec dsimplex22(arma::vec x, double mu, double dispersion, bool loga) {
    double sigma=dispersion;
    arma::vec res;
    
    arma::vec logpdf=(-0.5 * log(2 * arma::datum::pi) - log(sigma) - 1.5 * log(x) - 1.5 * log(-x+1) - 0.5 * deeFun(x, mu)/pow(sigma,2));
    if(loga)
    {
        res=logpdf;
        }
        else {
            res=exp(logpdf);
    }

    arma::uvec indices=arma::find(x<0);

    return(res);

}   


// [[Rcpp::export]]
arma::vec dsimplex2(arma::vec x, double mu, double dispersion, bool loga) {
    double sigma=dispersion;
    arma::vec res;
    
    arma::vec logpdf=(-0.5 * log(2 * arma::datum::pi) - log(sigma) - 1.5 * log(x) - 1.5 * log(-x+1) - 0.5 * deeFun(x, mu)/pow(sigma,2));
    if(loga)
    {
        res=logpdf;
        }
        else {
            res=exp(logpdf);
    }

    arma::uvec indices=arma::find(x<=0);
    if(indices.n_elem>0)
    {
        for(int i=0; i<indices.n_elem; i++)
        res[indices[i]]=-arma::datum::inf;
    }

    indices=arma::find(x>=1);
    if(indices.n_elem>0)
    {
        for(int i=0; i<indices.n_elem; i++)
        res[indices[i]]=-arma::datum::inf;
    }


    if(mu<0 | mu>1)
    {
        for(int i=0; i<res.n_elem; i++)
        res[i]=arma::datum::nan;
    }

    if(sigma<=0)
    {
        for(int i=0; i<res.n_elem; i++)
        res[i]=arma::datum::nan;
    }

    return(res);

}   



// [[Rcpp::export]]
double den(
    arma::vec y,
    arma::vec pj,
    arma::vec para
)

{
    double den=0;
    int K=pj.n_elem;
    // int N=y.n_elem;
    
    arma::vec yy(1);

/*    
    for(int s=0; s<K; s++)
    {
        para[s]=exp(para[s])/(1+exp(para[s]));
        // para[s+K]=exp(para[s+K]);
    }    
*/

    for(int k=0; k<K; k++)
    {
        yy=y[0];   
        den=den+pj[k]*dsimplex2(yy,para[k],para[k+K], 0)[0];
        
    }

 
    return(den);
}


// [[Rcpp::export]]
arma::mat peij(
    arma::vec y,
    arma::vec pj,
    arma::vec para
)

{

    int K=pj.n_elem;
    int N=y.n_elem;
    double denominador;
    arma::mat pij=arma::zeros<arma::mat>(N,K);
    arma::vec yy(1);

/*
    for(int s=0; s<K; s++)
    {
        para[s]=exp(para[s])/(1+exp(para[s]));
        // para[s+K]=exp(para[s+K]);
    }
*/
        
    for(int i=0; i<N; i++)
    {
        yy=y[i];     
        denominador = den(yy, pj, para);      
        
        for(int s=0; s<K; s++)
            {
                pij(i,s)=pj[s]*dsimplex2(yy,para[s],para[s+K], 0)[0]/denominador;
            }
    }
    
    return(pij);

}



// [[Rcpp::export]]
double fn(
    arma::vec y,
    arma::vec pj,
    arma::vec para,
    arma::mat pij
)

{

    int K=pj.n_elem;
    int N=y.n_elem;
    double accum=0;
    
    arma::vec yy(1);    

    for(int s=0; s<K; s++) // inv.logit?
    {
        para[s]=exp(para[s])/(1+exp(para[s]));
        para[s+K]=exp(para[s+K]);
    }

    for(int i=0; i<N; i++)
    {
        yy=y[i];      

            for(int s=0; s<K; s++)
                {
                    accum=accum+pij(i,s)*(log(pj[s])+dsimplex2(yy,para[s],para[s+K], 1)[0]);
                }        
    }
        
    return(-accum);

}


// [[Rcpp::export]]
arma::vec fn_ind(
    arma::vec y,
    arma::vec pj,
    arma::vec para,
    arma::mat pij
)

{

    int K=pj.n_elem;
    int N=y.n_elem;
    arma::mat res(N,K);

    arma::vec yy(1);

    for(int s=0; s<K; s++)
    {
        para[s]=exp(para[s])/(1+exp(para[s]));
        para[s+K]=exp(para[s+K]);
    }

    for(int i=0; i<N; i++)
    {
        yy=y[i];

            for(int s=0; s<K; s++)
                {
                    res(i,s)=pij(i,s)*(log(pj[s])+dsimplex2(yy,para[s],para[s+K], 1)[0]);
                }
    }

    arma::vec res2=sum(res,1);

    return(res2);

}

