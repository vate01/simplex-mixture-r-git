#include <RcppArmadillo.h>
using namespace Rcpp;
// [[Rcpp::depends("RcppArmadillo")]]
#include <RcppArmadilloExtensions/sample.h>


// [[Rcpp::export]]
arma::vec dbeta22(arma::vec x, double mu, double phi, bool loga) {

int K=x.n_elem;
    arma::vec res(K);

    for(int k=0; k<K; k++)
    {
        //yy=y[0];   
        res[k]=::Rf_dbeta(x(k), mu*phi, phi*(1-mu), loga);
        
    }

    return(res);

}  

// [[Rcpp::export]]
double den_zim_beta(
    arma::vec y,
    arma::vec pj,
    arma::vec para
)

{
    double den=0;
    int K=pj.n_elem-1;
    int N=y.n_elem;
    
    arma::vec yy(1);

    if ( pj.n_elem != ((para.n_elem)/2 +1) ) {  
        throw std::range_error("dimensiones pj");
    }
    
    for(int i=0; i<N; i++)
    {
        if(y[i]==0)
        {
            den=den+pj[0];
        }
        
        if(y[i]!=0)
        {
            for(int k=1; k<(K+1); k++)
            {
                yy=y[i];
                den=den+pj[k]*dbeta22(yy,para[k-1],para[k+K-1], 0)[0];
            }
        }
        
    }
    
    return(den);
}


// [[Rcpp::export]]
arma::mat peij_zim_beta(
    arma::vec y,
    arma::vec pj,
    arma::vec para
)

{

    int K=pj.n_elem-1;
    int N=y.n_elem;
    double denominador;
    arma::mat pij=arma::zeros<arma::mat>(N,K+1);
    arma::vec yy(1);
    
    if ( pj.n_elem != ((para.n_elem)/2 +1) ) {  
        throw std::range_error("dimensiones pj");
    }
        
    for(int i=0; i<N; i++)
    {
        yy=y[i];     
        denominador = den_zim_beta(yy, pj, para); 
        
        if(y[i]==0.0)
        {          
            pij(i,0)=1;

        }
        
        if (y[i]>0)
        {
            pij(i,0)=0;       
            
            for(int s=1; s<(K+1); s++)
                {
                    pij(i,s)=pj[s]*dbeta22(yy,para[s-1],para[s+K-1], 0)[0]/denominador;
                }
        }

    }
    
    return(pij);

}


// [[Rcpp::export]]
double fn_zim_beta(
    arma::vec y,
    arma::vec pj,
    arma::vec para,
    arma::mat pij
)

{

    int K=pj.n_elem-1;
    int N=y.n_elem;
    double accum=0;
    
    arma::vec yy(1);    

    for(int s=1; s<(K+1); s++)
    {
        para[s-1]=exp(para[s-1])/(1+exp(para[s-1]));
        para[s+K-1]=exp(para[s+K-1]);
    }

    for(int i=0; i<N; i++)
    {
        yy=y[i];      
        if(y[i]==0.0)
        {
            accum=accum+pij(i,0)*log(pj[0]);
        }

        if (y[i]>0)
        {
            for(int s=1; s<(K+1); s++)
                {
                    accum=accum+pij(i,s)*(log(pj[s])+dbeta22(yy,para[s-1],para[s+K-1], 1)[0]);
                }        
        }
          
    }
    
        
    return(-accum);

}


// [[Rcpp::export]]
arma::vec fn_zim_ind_beta(
    arma::vec y,
    arma::vec pj,
    arma::vec para,
    arma::mat pij
)

{

    int K=pj.n_elem-1;
    int N=y.n_elem;
    arma::mat  res = arma::zeros<arma::mat>(N, K+1);

    for(int s=1; s<(K+1); s++)
    {
        para[s-1]=exp(para[s-1])/(1+exp(para[s-1]));
        para[s+K-1]=exp(para[s+K-1]);
    }
    
    arma::vec yy(1);    

    for(int i=0; i<N; i++)
    {
        yy=y[i];      
        if(y[i]==0.0)
        {
            res(i,0)=pij(i,0)*log(pj[0]);
        }

        if (y[i]>0)
        {
            for(int s=1; s<(K+1); s++)
                {
                    res(i,s)=pij(i,s)*(log(pj[s])+dbeta22(yy,para[s-1],para[s+K-1], 1)[0]);
                }        
        }
          
    }
    
    arma::vec res2=sum(res,1);
            
    return(res2);

}
