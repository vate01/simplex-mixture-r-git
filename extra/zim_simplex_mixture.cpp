#include <RcppArmadillo.h>
using namespace Rcpp;
// [[Rcpp::depends("RcppArmadillo")]]
#include <RcppArmadilloExtensions/sample.h>


/*
    logpdf[x <= 0] <- -Inf
    logpdf[x >= 1] <- -Inf
    logpdf[mu <= 0] <- NaN
    logpdf[mu >= 1] <- NaN
    logpdf[sigma <= 0] <- NaN
*/


// [[Rcpp::export]]
arma::vec deeFun(arma::vec y, double mu) {

    arma::vec A= pow((y - mu)/(mu * (1 - mu)),2) ;
    arma::vec B= (y % (1 - y));
    arma::vec C=A/B;

return(C);
}  
    
// [[Rcpp::export]]
arma::vec dsimplex22(arma::vec x, double mu, double dispersion, bool loga) {
    double sigma=dispersion;
    arma::vec res;
    
    arma::vec logpdf=(-0.5 * log(2 * arma::datum::pi) - log(sigma) - 1.5 * log(x) - 1.5 * log(-x+1) - 0.5 * deeFun(x, mu)/pow(sigma,2));
    if(loga)
    {
        res=logpdf;}
        else {
            res=exp(logpdf);
    }

arma::uvec indices=arma::find(x<0);
//res.elem(indices)=2;

return(res);

}   


// [[Rcpp::export]]
arma::vec dsimplex2(arma::vec x, double mu, double dispersion, bool loga) {
    double sigma=dispersion;
    arma::vec res;
    
    arma::vec logpdf=(-0.5 * log(2 * arma::datum::pi) - log(sigma) - 1.5 * log(x) - 1.5 * log(-x+1) - 0.5 * deeFun(x, mu)/pow(sigma,2));
    if(loga)
    {
        res=logpdf;
        }
        else {
            res=exp(logpdf);
    }

    /*
        logpdf[x <= 0] <- -Inf
        logpdf[x >= 1] <- -Inf
        logpdf[mu <= 0] <- NaN
        logpdf[mu >= 1] <- NaN
        logpdf[sigma <= 0] <- NaN
    */

    arma::uvec indices=arma::find(x<=0);
    if(indices.n_elem>0)
    {
        for(int i=0; i<indices.n_elem; i++)
        res[indices[i]]=-arma::datum::inf;
    }

    indices=arma::find(x>=1);
    if(indices.n_elem>0)
    {
        for(int i=0; i<indices.n_elem; i++)
        res[indices[i]]=-arma::datum::inf;
    }


    if(mu<0 | mu>1)
    {
        for(int i=0; i<res.n_elem; i++)
        res[i]=arma::datum::nan;
    }

    if(sigma<=0)
    {
        for(int i=0; i<res.n_elem; i++)
        res[i]=arma::datum::nan;
    }

    return(res);

}   



// [[Rcpp::export]]
double den_zim(
    arma::vec y,
    arma::vec pj,
    arma::vec para
)

{
    double den=0;
    int K=pj.n_elem-1;
    int N=y.n_elem;
    
    arma::vec yy(1);

    if ( pj.n_elem != ((para.n_elem)/2 +1) ) {  
        throw std::range_error("dimensiones pj");
    }
    
    for(int i=0; i<N; i++)
    {
        if(y[i]==0)
        {
            den=den+pj[0];
        }
        
        if(y[i]!=0)
        {
            for(int k=1; k<(K+1); k++)
            {
                yy=y[i];
                den=den+pj[k]*dsimplex2(yy,para[k-1],para[k+K-1], 0)[0];
            }
        }
        
    }
    
    return(den);
}


// [[Rcpp::export]]
arma::mat peij_zim(
    arma::vec y,
    arma::vec pj,
    arma::vec para
)

{

    int K=pj.n_elem-1;
    int N=y.n_elem;
    double denominador;
    arma::mat pij=arma::zeros<arma::mat>(N,K+1);
    arma::vec yy(1);
    
    if ( pj.n_elem != ((para.n_elem)/2 +1) ) {  
        throw std::range_error("dimensiones pj");
    }
        
    for(int i=0; i<N; i++)
    {
        yy=y[i];     
        denominador = den_zim(yy, pj, para); 
        
        if(y[i]==0.0)
        {   
        //yy=y[i];     
        //denominador = den_zim(yy, pj, para);        
            pij(i,0)=1;//pj[0]/denominador;
            /*
            for(int t=1; t<(K+1); t++) 
                {
                    pij[i,t]=0;
                }*/

        }
        
        if (y[i]>0)
        {
            pij(i,0)=0;       
            
            for(int s=1; s<(K+1); s++)
                {
                    pij(i,s)=pj[s]*dsimplex2(yy,para[s-1],para[s+K-1], 0)[0]/denominador;
                }
        }

    }
    
    return(pij);

}


// [[Rcpp::export]]
double fn_zim(
    arma::vec y,
    arma::vec pj,
    arma::vec para,
    arma::mat pij
)

{

    int K=pj.n_elem-1;
    int N=y.n_elem;
    double accum=0;

    if ( pj.n_elem != ((para.n_elem)/2 +1) ) {  
        throw std::range_error("dimensiones pj");
    }
    
    for(int s=1; s<(K+1); s++)
    {
        para[s-1]=exp(para[s-1])/(1+exp(para[s-1]));
        para[s+K-1]=exp(para[s+K-1]);
    }
 
    arma::vec yy(1);    

    for(int i=0; i<N; i++)
    {
        yy=y[i];      
        if(y[i]==0.0)
        {
            accum=accum+pij(i,0)*log(pj[0]);
        }

        if (y[i]>0)
        {
            for(int s=1; s<(K+1); s++)
                {
                    accum=accum+pij(i,s)*(log(pj[s])+dsimplex2(yy,para[s-1],para[s+K-1], 1)[0]);
                }        
        }
          
    }
    
        
    return(-accum);

}


// [[Rcpp::export]]
arma::vec fn_zim_ind(
    arma::vec y,
    arma::vec pj,
    arma::vec para,
    arma::mat pij
)

{

    int K=pj.n_elem-1;
    int N=y.n_elem;
    arma::mat  res = arma::zeros<arma::mat>(N, K+1);

    for(int s=1; s<(K+1); s++)
    {
        para[s-1]=exp(para[s-1])/(1+exp(para[s-1]));
        para[s+K-1]=exp(para[s+K-1]);
    }
    
    arma::vec yy(1);    

    for(int i=0; i<N; i++)
    {
        yy=y[i];      
        if(y[i]==0.0)
        {
            res(i,0)=pij(i,0)*log(pj[0]);
        }

        if (y[i]>0)
        {
            for(int s=1; s<(K+1); s++)
                {
                    res(i,s)=pij(i,s)*(log(pj[s])+dsimplex2(yy,para[s-1],para[s+K-1], 1)[0]);
                }        
        }
          
    }
    
    arma::vec res2=sum(res,1);
            
    return(res2);

}
